// console.log("Heloo World");


/*
	OBJECTS
		- an object is a data type that us used to represent real world objects. It is also a collection of related data and/or functionalities.

	Object Literals
		- one of the methods in creating objects.

		Syntax:
			let objectName ={
				keyA: valueA,
				keyB: valueB,
			}

*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999

};

console.log("Result from creating object using Object Literals:");
console.log(cellphone);
console.log(typeof cellphone);


// Creating Objects Using a Constructor Function (Object Constructor)
/*
	Creates a reusable function to create severa; objects that have the same data structure. This is useful for creating multiple instance/copies of an object.


	Syntax:
		function ObjectName(valueA, ValueB){
			this.keyA = valueA;
			this.keyB = valueB
		};

		let varableName = new function ObjectName(valueA, valueB);
		console.log(variableName);

		=============================
		-this is for invoking; it refers to the global object.
		-don't forget the "new" keyword when creatin a new object.
*/
// We use PascalCase for the Constructor Function Name.
function Laptop(name, manufactureDate){
	this.name = name
	this.manufactureDate = manufactureDate
};

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using Constructor function:");
console.log(laptop);

let myLaptop = new Laptop("MacBook Air",[2020,2021]);
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC" ,1980);
console.log("Result of creating objects without the new kyword:")
console.log(oldLaptop);



//Creating empty objects as placeholder.

let computer = {};
let myComputer = new Object();

console.log(computer);
console.log(myComputer);


myComputer = {
	name: "Asus",
	manufactureDate: 2012
};

console.log(myComputer);

function Car (brand,model,type){
	this.brand = brand
	this.model = model
	this.type = type

};

let car = new Car("Honda",2020,"Crossover")
console.log(car)

let myCar = new Car("Suzuki", 2022,"Pick-up")
console.log(myCar);

// Accessing Object Properties

// Using the dot notation
console.log("Result from dot notation: " + myLaptop.name);

// Using The square bracket notation
console.log("Result from square bracket nottaion: "+myLaptop["name"]);

// Accessing array of objects
let deviceArr = [laptop,myLaptop];
// let deviceArr =[{name:Lenovo, manufactureDate: 2008}, {name: mackbook air, ,aunfacyureDate: 2020}];

// Dot Notation
console.log(deviceArr[0].manufactureDate);

//Square Bracket Notation
console.log(deviceArr[0]["manufactureDate"]);


// Initializing /Adding /Deleting /Reassigning Object Properties
// (CRUD Operations)

car = {};
console.log(car);

// Adding Object
car.name = "Honda Civic";
console.log	("Result from adding property using dot notation");
console.log(car);

car["manufacture date"] = 2019;
console.log	("Result from adding property using dot notation");
console.log(car);

// Deleting object properties
delete car ["manufacture date"];
console.log(" Result from deleting object properties");
console.log(car);

// Reassigning object properties
car.name="Tesla";
console.log("Result from reassigning property:")
console.log(car);


// Object Methods

/*
	This method is a function which is stored in an object property. Ther are also functions and one of the key difference that they have is that methods are functions related to a specific object.
*/

let person = {
	name: "john",
	age:25,
	talk:function(){
		console.log("Hello ! My Name is " + this.name)
	}
};

console.log(person);
console.log("Result from Object Methods: ");
person.talk();

person.walk = function(){
	console.log(this.name + "have walked 25 steps forward.")
};
person.walk();

let friend = {
	firstName: "Jane",
	lastName: "Doe",
	address:{
		city: "Austin, Texas",
		conutry: "US"
	},
	emails:["janedoe@gmail.com", "jane121992@gmail.com"],
	introduce:function(){
		console.log("Hello! My name is "+this.firstName + ""+this.lastName+ ". " + "I live in" + this.address.city+","+this.address.conutry+".")
	}
};
friend.introduce();

// Real World Application Objects

// Using Object Literals

// let myPokemon = {
// 	name: "Pikachu",
// 	level: 3,
// 	health: 100,
// 	attack: 15,
// 	tackle:function(){
// 		console.log(this.name + "tackled targetPokemon")
// 		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
// 	},
// 	faint:function(){
// 		console.log(this.name+"fainted")
// 	}
// };
// console.log(myPokemon);
// myPokemon.tackle();

// // Creating real world object using constructor function
// function Pokemon(name,level){
// 	this.name = name
// 	this.level = level
// 	this.health = 5 * level
// 	this.attack = 2 * level

// 	// methods
// 	this.tackle = function(target){
// 		console.log(this.name + " tackled "+ target.name)
// 		console.log(target.name + "'s health is now reduce to " +(target.health - this.attack))
// 	},

// 	this.faint = function(){
// 		console.log(this.name + " fainted.")
// 	}
// };

// let charmander = new Pokemon("charmander",12);
// let squirtle = new Pokemon("Squirtle", 6);
// console.log(charmander);
// console.log(squirtle);

// charmander.tackle(squirtle);


/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 0, invoke faint function


*/


// Creating real world object using constructor function
function Pokemon(name,level){
	this.name = name
	this.level = level
	this.health = 10*level
	this.attack = 2 * level
	// this.life = health - attack

	// methods
	this.tackle = function(target){
		if(target.health -= this.attack){
			console.log(this.name + " tackled "+ target.name)
			console.log(target.name+ "'s health is now reduce to " +(target.health ))
		}

		
		



	},

	this.faint = function(fainted){
		
		console.log(this.name + " fainted.")
	}
};

let snorlax = new Pokemon("Snorlax",7 ,);
let charizard = new Pokemon("Charizard", 5);
console.log(snorlax);
console.log(charizard);




snorlax.tackle(charizard);
charizard.tackle(snorlax);
snorlax.tackle(charizard);
charizard.tackle(snorlax);
snorlax.tackle(charizard);
charizard.tackle(snorlax);
snorlax.tackle(charizard);
charizard.faint();

console.log(charizard);
console.log(snorlax);




